from .models import *
from rest_framework.serializers import ModelSerializer

class PokemonTypeSerializer(ModelSerializer):

    class Meta:
        model = PokemonType
        fields = ( 'id', 'name', 'url', )

class PokemonSlotSerializer(ModelSerializer):
    type = PokemonTypeSerializer()

    class Meta:
        model = PokemonSlot
        fields = ( 'id', 'slot', 'type', )

class GameVersionSerializer(ModelSerializer):
    
    class Meta:
        model = GameVersion
        fields = ( 'id', 'name', 'url', )

class GameIndexSerializer(ModelSerializer):
    version = GameVersionSerializer()

    class Meta:
        model = GameIndex
        fields = ( 'id', 'game_index', 'version', )

class PokemonFormsSerializer(ModelSerializer):
   
    class Meta:
        model = PokemonForms
        fields = ( 'id', 'name', 'url', )

class PokemonSerializer(ModelSerializer):
    types = PokemonSlotSerializer(many=True)
    game_indices = GameIndexSerializer(many=True)
    forms = PokemonFormsSerializer(many=True)

    class Meta:
        model = Pokemon
        fields = ( 'id', 'name', 'order', 'height', 'types', 'game_indices', 'forms', )

    def create(self, validated_data):
        pokemon_instance = Pokemon.objects.create(name=validated_data.get("name"), order=validated_data.get("order"), height = validated_data.get("height"))
        for form in validated_data.get("forms"):
            pokemon_instance.forms.add(PokemonForms.objects.get_or_create(form)[0])

        for indices in validated_data.get("game_indices"):
            pokemon_instance.game_indices.add(GameIndex.objects.get_or_create(
                game_index=indices.get("game_index"), version=GameVersion.objects.get_or_create(**indices.get("version"))[0])[0]
                )

        for types in validated_data.get("types"):
            pokemon_instance.types.add(PokemonSlot.objects.get_or_create(
                slot=types.get("slot"), type=PokemonType.objects.get_or_create(**types.get("type"))[0])[0]
                )
        pokemon_instance.save()   
        return pokemon_instance

    def update(self, instance, validated_data):
        instance.order = validated_data.get("order")
        instance.height = validated_data.get("height")
        for form in validated_data.get("forms"):
            instance.forms.add(PokemonForms.objects.get_or_create(form)[0])

        for indices in validated_data.get("game_indices"):
            instance.game_indices.add(GameIndex.objects.get_or_create(
                game_index=indices.get("game_index"), version=GameVersion.objects.get_or_create(**indices.get("version"))[0])[0]
                )

        for types in validated_data.get("types"):
            instance.types.add(PokemonSlot.objects.get_or_create(
                slot=types.get("slot"), type=PokemonType.objects.get_or_create(**types.get("type"))[0])[0]
                )

        instance.save()   
        return instance