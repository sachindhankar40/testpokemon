from django.contrib import admin
from .models import *

admin.site.register(Pokemon)
admin.site.register(PokemonForms)
admin.site.register(PokemonType)
admin.site.register(PokemonSlot)
admin.site.register(GameVersion)
admin.site.register(GameIndex)