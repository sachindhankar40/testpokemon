import asyncio
import aiohttp
import json
import requests
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from django.conf import settings
from rest_framework.exceptions import bad_request

from pokemon.models import Pokemon
from .serializers import PokemonSerializer
from asgiref.sync import sync_to_async

@sync_to_async
def save_pokemon(result):
    result = {
        "name":result.pop("name"),
        "order" : result.pop("order"),
        "height" : result.pop("height"),
        "types" : result.pop("types"),
        "game_indices" : result.pop("game_indices"),
        "forms" : result.pop("forms")
    }
    serializer_var = PokemonSerializer(data=result)
    if serializer_var.is_valid(raise_exception=True):
        serializer_var.save()

async def get(url, session):
        try:
            async with session.get(url=url) as response:
                retreive_reaponse = await response.json()
                result = retreive_reaponse
                await save_pokemon(result)
        except Exception as e:
            print("Unable to get url {} due to {}.".format(url, e.__class__))

async def main(urls):
    async with aiohttp.ClientSession() as session:
        tasks = []
        for url in urls:
            task = asyncio.ensure_future(get(url, session))
            tasks.append(task)

        view_counts = await asyncio.gather(*tasks)

class CrowlPokemon(APIView):
    authentication_classes = ()
    permission_classes = ()    

    def get(self, request, *args, **kwargs):
        list_response = requests.get(url=settings.POKEMON.get('POKEMON_LIST'))
        if list_response.status_code == 200:
            urls = [settings.POKEMON.get('POKEMON_LIST')+f'/{result.get("name")}' for result in list_response.json().get("results")]
            asyncio.run(main(urls))
        else:
            raise bad_request({"error":"Something went wrong on pokemon api."})

class PokemonView(ModelViewSet):
    permission_classes = ()
    authentication_classes = ()
    queryset = Pokemon.objects.all()
    serializer_class = PokemonSerializer
    lookup_field = 'name'