from pyexpat import model
from django.db import models

class PokemonType(models.Model):
    name = models.CharField(max_length=150)
    url = models.TextField()

class PokemonSlot(models.Model):
    slot = models.IntegerField(default=0)
    type = models.ForeignKey(PokemonType, on_delete=models.CASCADE)

class GameVersion(models.Model):
    name = models.CharField(max_length=150)
    url = models.TextField()

class GameIndex(models.Model):
    game_index = models.IntegerField(default=0)
    version = models.ForeignKey(GameVersion, on_delete=models.CASCADE)

class PokemonForms(models.Model):
    name = models.CharField(max_length=150)
    url = models.TextField()

class Pokemon(models.Model):
    name = models.CharField(max_length=150, unique=True)
    order = models.BigIntegerField(default=0)
    height = models.IntegerField(default=0)
    types = models.ManyToManyField(PokemonSlot)
    game_indices = models.ManyToManyField(GameIndex)
    forms = models.ManyToManyField(PokemonForms)

    def __str__(self) -> str:
        return self.name